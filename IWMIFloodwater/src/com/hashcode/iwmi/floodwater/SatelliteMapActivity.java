package com.hashcode.iwmi.floodwater;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.esri.android.map.Callout;
import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISDynamicMapServiceLayer;
import com.esri.android.map.ags.ArcGISTiledMapServiceLayer;
import com.esri.android.map.event.OnLongPressListener;
import com.esri.android.map.event.OnSingleTapListener;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.PictureMarkerSymbol;
import com.hashcode.iwmi.floodwater.model.MarkerPoint;
import com.hashcode.iwmi.floodwater.tasks.MarkerDataLoadAsyncTask;
import com.hashcode.iwmi.floodwater.tasks.MarkerDataLoadAsyncTask.OnTaskComplete;
import com.hashcode.iwmi.floodwater.util.HttpUtil;

public class SatelliteMapActivity extends FragmentActivity
{
    private MapView mMapView;
    private ArcGISTiledMapServiceLayer tileLayer;
    private ArcGISDynamicMapServiceLayer mStreetsLayer;
    private GraphicsLayer graphicsLayer;
    private Callout callout;

    private List<MarkerPoint> markerPoints = new ArrayList<MarkerPoint>();
    private SparseArray<MarkerPoint> markerIdMap = new SparseArray<MarkerPoint>(10);
    private MarkerPoint mostNearestPoint = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_satellite_map);

        try
        {
            if (!HttpUtil.isConnected(this))
            {
                Toast.makeText(getApplicationContext(), "Please check your network connection", Toast.LENGTH_SHORT)
                    .show();
            }
            // load the map points.
            if (markerPoints.isEmpty())
            {
                MarkerDataLoadAsyncTask markerDataTask = new MarkerDataLoadAsyncTask(this.getApplicationContext());
                markerDataTask.execute(getString(R.string.fw_server_url), getString(R.string.fw_load_markers_url));
                markerDataTask.setTaskCompleteListener(new OnTaskComplete()
                {

                    @Override
                    public void setMyTaskComplete(List<MarkerPoint> points)
                    {
                        markerPoints = points;
                        addMarkerGraphic();
                    }

                });
            }
            initilizeMap();
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(), "Error [" + e.getLocalizedMessage() + "]", Toast.LENGTH_SHORT)
                .show();
        }
    }

    /**
     * <p>
     * Function to load the map with overlays.
     * </p>
     * 
     */
    private void initilizeMap()
    {
        Log.i("FW", "Initilizing Flood water Map [ ~ Satellite ~ ]");

        mMapView = (MapView) findViewById(R.id.satelliteMap);
        callout = mMapView.getCallout();

        tileLayer = new ArcGISTiledMapServiceLayer(getString(R.string.arc_gis_world_satellite_map));
        mMapView.addLayer(tileLayer);

        mStreetsLayer = new ArcGISDynamicMapServiceLayer(getString(R.string.arc_gis_nigeria_main_view));
        mStreetsLayer.setOpacity(0.6f);
        mMapView.addLayer(mStreetsLayer);

        tileLayer.setOnStatusChangedListener(onMapLayerStatusChange());
        mMapView.setOnSingleTapListener(mapSingleTapListener());
        mMapView.setOnLongPressListener(mapLongPressListener());

        Log.i("FW", "Flood water map data loaded successfully");
    }

    private OnStatusChangedListener onMapLayerStatusChange()
    {
        return new OnStatusChangedListener()
        {
            private static final long serialVersionUID = 6939736191734632383L;

            public void onStatusChanged(Object source, STATUS status)
            {
                if (OnStatusChangedListener.STATUS.INITIALIZED == status)
                {
                    // Add Markers layer to map
                    if (!markerPoints.isEmpty())
                        addMarkerGraphic();
                }
            }
        };
    }

    private OnLongPressListener mapLongPressListener()
    {
        return new OnLongPressListener()
        {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean onLongPress(float x, float y)
            {
                if (callout.isShowing())
                {
                    callout.hide();
                }
                return false;
            }
        };
    }

    private OnSingleTapListener mapSingleTapListener()
    {
        return new OnSingleTapListener()
        {
            private static final long serialVersionUID = -8298989679060594521L;

            public void onSingleTap(float lat, float lon)
            {
                int[] graphicsAtTapPoint = graphicsLayer.getGraphicIDs(lat, lon, 10);

                if (graphicsAtTapPoint != null && graphicsAtTapPoint.length > 0)
                {
                    Graphic foundGraphic = graphicsLayer.getGraphic(graphicsAtTapPoint[0]);
                    int id = foundGraphic.getUid();

                    Point graphic = (Point) foundGraphic.getGeometry();
                    mostNearestPoint = markerIdMap.get(id);
                    showCallout(graphic);
                }
                else
                {
                    if (callout.isShowing())
                    {
                        callout.hide();
                    }
                }
            }
        };
    }
    private static final DecimalFormat numFormat = new DecimalFormat("#.00");
    
    @SuppressLint("InflateParams")
    private void showCallout(Point graphic)
    {
        View locationCallout = getLayoutInflater().inflate(R.layout.callout_layout, null);
        TextView calloutTitle = (TextView) locationCallout.findViewById(R.id.calloutTitle);
        calloutTitle.setText(mostNearestPoint.getName());

        TextView calloutContent = (TextView) locationCallout.findViewById(R.id.calloutContent);
        calloutContent.setText(getString(R.string.water_level_label) + " :" + numFormat.format(mostNearestPoint.getWaterLevel()) + "" + getString(R.string.meter));
        calloutContent.setMaxLines(2);

        locationCallout.findViewById(R.id.callout_layout).setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    TextView pointTitle = (TextView) v.findViewById(R.id.calloutTitle);
                    String pointName = (String) pointTitle.getText();

                    Intent intent = new Intent(getApplicationContext(), SummaryViewActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("name", pointName);
                    startActivity(intent);
                    finish();
                }
                catch (Throwable t)
                {
                    Toast.makeText(getApplicationContext(), "Marker Clicked ERROR :" + t.getMessage(),
                        Toast.LENGTH_SHORT).show();
                }
            }
        });

        callout.show(graphic, locationCallout);
    }

    public void addMarkerGraphic()
    {
        graphicsLayer = new GraphicsLayer();
        markerIdMap.clear();

        for (MarkerPoint marker : markerPoints)
        {
            Point point =
                (Point) GeometryEngine.project(marker.getPoint(), SpatialReference.create(4326),
                    mMapView.getSpatialReference());

            // Default marker symbol;
            PictureMarkerSymbol pictureMarkerSymbol =
                new PictureMarkerSymbol(getResources().getDrawable(R.drawable.marker_blue));
            if (marker.isModerate())
            {
                pictureMarkerSymbol = new PictureMarkerSymbol(getResources().getDrawable(R.drawable.marker_orange));
            }
            else if (marker.isRisk())
            {
                pictureMarkerSymbol = new PictureMarkerSymbol(getResources().getDrawable(R.drawable.marker_red));
            }

            Graphic markerGraphic = new Graphic(point, pictureMarkerSymbol);
            int id = graphicsLayer.addGraphic(markerGraphic);
            markerIdMap.put(id, marker);

        }
        mMapView.addLayer(graphicsLayer);
    }

    @Override
    public void onBackPressed()
    {
        Intent backIntent = new Intent();
        backIntent.setClass(this, MainFullscreenActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(backIntent);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        initilizeMap();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return true;
    }

}
