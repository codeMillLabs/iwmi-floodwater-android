package com.hashcode.iwmi.floodwater;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

@SuppressWarnings("deprecation")
public class MapTabActivity extends TabActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_map_tab);

        final TabHost tabHost = getTabHost();

        // Info tab
        Intent infoIntent = new Intent().setClass(this, InfomationActivity.class);
        TabSpec infoTabSpec =
            tabHost.newTabSpec("Info").setIndicator(getText(R.string.tab_info), null).setContent(infoIntent);

        // Standard Map view tab
        final Intent standardMapIntent = new Intent().setClass(this, MapActivity.class);
        final TabSpec standardTabSpec =
            tabHost.newTabSpec("Standard").setIndicator(getText(R.string.tab_standard), null)
                .setContent(standardMapIntent);

        // Satellite view tab
        final Intent satelliteMapIntent = new Intent().setClass(this, SatelliteMapActivity.class);
        final TabSpec satelliteTabSpec =
            tabHost.newTabSpec("Satellite").setIndicator(getText(R.string.tab_satellite), null)
                .setContent(satelliteMapIntent);

        // Hybrid map view tab
        final Intent hybridMapIntent = new Intent().setClass(this, HybridMapActivity.class);
        final TabSpec hybridTabSpec =
            tabHost.newTabSpec("Hybrid").setIndicator(getText(R.string.tab_hybrid), null).setContent(hybridMapIntent);

        // add all tabs 
        tabHost.addTab(infoTabSpec);
        tabHost.addTab(standardTabSpec);
        tabHost.addTab(satelliteTabSpec);
        tabHost.addTab(hybridTabSpec);

        //set Windows tab as default (zero based)
        if (getIntent().getExtras() != null && getIntent().getExtras().get("tab_index") != null)
        {
            tabHost.setCurrentTab((Integer) getIntent().getExtras().get("tab_index"));
        }
        else
            tabHost.setCurrentTab(1);

        tabHost.setOnTabChangedListener(new OnTabChangeListener()
        {
            @Override
            public void onTabChanged(String tabId)
            {

                if ("Satellite".equals(tabId))
                {
                    Intent tabIntent = new Intent();
                    tabIntent.setClass(getApplicationContext(), MapTabActivity.class);
                    tabIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    tabIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    tabIntent.putExtra("tab_index", 2);
                    startActivity(tabIntent);
                    finish();
                }
                else if ("Hybrid".equals(tabId))
                {
                    Intent tabIntent = new Intent();
                    tabIntent.setClass(getApplicationContext(), MapTabActivity.class);
                    tabIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    tabIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    tabIntent.putExtra("tab_index", 3);
                    startActivity(tabIntent);
                    finish();
                }
                else if ("Standard".equals(tabId))
                {
                    Intent tabIntent = new Intent();
                    tabIntent.setClass(getApplicationContext(), MapTabActivity.class);
                    tabIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    tabIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    tabIntent.putExtra("tab_index", 1);
                    startActivity(tabIntent);
                    finish();
                }
                else
                {
                    tabHost.setCurrentTab(0);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_settings)
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        Intent backIntent = new Intent();
        backIntent.setClass(this, MainFullscreenActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(backIntent);
        finish();
        super.onBackPressed();
    }
}
