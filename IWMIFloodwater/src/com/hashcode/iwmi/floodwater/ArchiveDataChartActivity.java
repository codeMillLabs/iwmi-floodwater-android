package com.hashcode.iwmi.floodwater;

import java.text.SimpleDateFormat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.androidplot.xy.XYPlot;
import com.hashcode.iwmi.floodwater.tasks.WaterDataSummaryAsyncTask;
import com.hashcode.iwmi.floodwater.util.DateFormat;
import com.hashcode.iwmi.floodwater.util.FWOnTouchListener;

@SuppressLint({
    "FloatMath", "ClickableViewAccessibility"
})
public class ArchiveDataChartActivity extends Activity implements OnClickListener
{
    @SuppressLint("SimpleDateFormat")
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM");
    private String measuringPoint = "";
    private TextView archiveTitle;
    private ImageButton backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_archive_data_chart);

        measuringPoint = getIntent().getExtras().getString("name");

        //initialize the page content
        archiveTitle = (TextView) findViewById(R.id.archiveChartTitle);
        archiveTitle.setText("Station : " + measuringPoint);

        TextView moderateText = (TextView) findViewById(R.id.moderateAText);
        moderateText.setText("/ Moderate");

        TextView riskText = (TextView) findViewById(R.id.riskAText);
        riskText.setText("/ Risk");

        TextView safeText = (TextView) findViewById(R.id.safeAText);
        safeText.setText("/ Safe");

        backBtn = (ImageButton) findViewById(R.id.archive_back_btn);
        backBtn.setOnClickListener(this);

        findViewById(R.id.map_view_btn).setOnClickListener(this);

        XYPlot xyPlot = loadXYPlotChart();
        FWOnTouchListener fwOnTouchListener = new FWOnTouchListener(xyPlot);
        xyPlot.setOnTouchListener(fwOnTouchListener);

        WaterDataSummaryAsyncTask asyncTask =
            new WaterDataSummaryAsyncTask(getApplicationContext(), xyPlot, measuringPoint, true, fwOnTouchListener);
        asyncTask.execute(getString(R.string.fw_server_url), getString(R.string.fw_water_level_archive_url), "true",
            "365");
    }

    private XYPlot loadXYPlotChart()
    {
        XYPlot xyPlot = (XYPlot) findViewById(R.id.archive_graph_layout);
        xyPlot.setDomainValueFormat(new DateFormat());
        /*
         * xyPlot.getLegendWidget().position(0, XLayoutStyle.ABSOLUTE_FROM_RIGHT, 10, YLayoutStyle.ABSOLUTE_FROM_TOP,
         * AnchorPosition.RIGHT_TOP);
         */
        xyPlot.getLayoutManager().remove(xyPlot.getLegendWidget());
        xyPlot.getLayoutManager().remove(xyPlot.getDomainLabelWidget());
        xyPlot.getLayoutManager().remove(xyPlot.getTitleWidget());
        return xyPlot;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.archive_back_btn:
                onBackPressed();
                break;

            case R.id.map_view_btn:
                loadMap();
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        Intent backIntent = new Intent();
        backIntent.setClass(this, MapTabActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(backIntent);
        finish();
        super.onBackPressed();
    }

    private void loadMap()
    {
        Intent backIntent = new Intent();
        backIntent.setClass(this, MapTabActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(backIntent);
        finish();

    }
}
