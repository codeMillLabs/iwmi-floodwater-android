/*
 * FILENAME
 *     GCMMessageHandlerService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.services;

import static com.hashcode.iwmi.floodwater.MapActivity.SHARED_PREF_NAME;
import android.annotation.TargetApi;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import com.hashcode.iwmi.floodwater.MapActivity;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Google cloud messages will handle by this service.
 * </p>
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 * @version $Id$
 **/
public class GcmMessageHandlerService extends IntentService
{

    private SharedPreferences settings;

    public GcmMessageHandlerService()
    {
        super("GCMMessageHandlerService");
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN) 
    @Override
    protected void onHandleIntent(Intent intent)
    {
        Bundle extras = intent.getExtras();
        if (extras != null)
        {
            String messageType = extras.getString("message_type");
            if (messageType.equals("MAP_SYNC"))
            {
                settings = getApplicationContext().getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(MapActivity.SYNC_MAP_DATA_KEY, true);
                editor.commit();
            }
            else if (messageType.equals("ALERT"))
            {
                PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);
                Notification notification =
                    new Notification.Builder(this).setContentTitle("Flood Water Alerts").setContentText("Subject")
                    //               .setSmallIcon(R.drawable.icon)
                        .setContentIntent(pIntent).setAutoCancel(true).build();

                NotificationManager notificationManager = 
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//                notificationManager.notify(19861214, notification); 
//                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
            }
        }

    }

}
