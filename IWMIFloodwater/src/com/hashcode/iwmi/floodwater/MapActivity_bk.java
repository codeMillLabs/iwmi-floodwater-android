package com.hashcode.iwmi.floodwater;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_HYBRID;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_TERRAIN;

import org.json.JSONArray;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hashcode.iwmi.floodwater.model.GeoCoordinate;
import com.hashcode.iwmi.floodwater.model.MapData;
import com.hashcode.iwmi.floodwater.model.PointPlacemark;
import com.hashcode.iwmi.floodwater.model.RoutePlacemark;
import com.hashcode.iwmi.floodwater.tasks.MapDataLoadAsyncTask;
import com.hashcode.iwmi.floodwater.util.HttpUtil;

public class MapActivity_bk extends FragmentActivity
{
    public static final String SHARED_PREF_NAME = "IWMI_FLOOD_WATER";
    public static final String LAST_MODIFIED_KEY = "lastModifiedDate";
    public static final String APP_VERSION = "app_version";
    public static final String GCM_REG_ID = "gcm_reg_id";
    public static final String MAP_DATA_KEY = "map_data";
    public static final String SYNC_MAP_DATA_KEY = "sync_map_data";

    // Google Map
    private GoogleMap googleMap;

    private SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        settings = getApplicationContext().getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);

        try
        {
            if (!HttpUtil.isConnected(this))
            {
                Toast.makeText(getApplicationContext(), "Please check your network connection", Toast.LENGTH_SHORT)
                    .show();
            }
            initilizeMap(MAP_TYPE_TERRAIN);

            if (settings.getString(MAP_DATA_KEY, "").isEmpty() || settings.getBoolean(SYNC_MAP_DATA_KEY, false))
            {
                MapDataLoadAsyncTask mapDataLoadTask = new MapDataLoadAsyncTask(this, googleMap, "Default");
                mapDataLoadTask.execute(getString(R.string.fw_server_url), getString(R.string.fw_load_modified_url));
            }

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(), "Error [" + e.getLocalizedMessage() + "]", Toast.LENGTH_SHORT)
                .show();
        }
    }

    /**
     * function to load map. If map is not created it will create it for you
     * */
    /**
     * <p>
     * Function to load the map with overlays.
     * </p>
     * 
     */
    private void initilizeMap(int googleMapType)
    {
        Log.i("FW", "Initilizing Floos water Map");

        if (googleMap == null)
        {
            googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

            // check if map is created successfully or not
            if (googleMap == null)
            {
                Toast.makeText(getApplicationContext(),
                    "Sorry! unable to initilize maps, Please check your connection", Toast.LENGTH_SHORT).show();
            }
            else
            {
                String mapDataJSONStr = settings.getString(MAP_DATA_KEY, "");

                if (!mapDataJSONStr.isEmpty())
                    populateKMLData(getApplicationContext(), googleMap, mapDataJSONStr);
                createOnMarkerClickListener();
            }
        }

        googleMap.setMapType(googleMapType);

        Log.i("FW", "Flood water map with KML data loaded successfully");
    }

    private void createOnMarkerClickListener()
    {
        googleMap.setOnMarkerClickListener(new OnMarkerClickListener()
        {

            @Override
            public boolean onMarkerClick(Marker marker)
            {
                try
                {
                    Intent intent = new Intent(getApplicationContext(), SummaryViewActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("name", marker.getTitle());
                    startActivity(intent);
                    finish();
                }
                catch (Throwable t)
                {
                    Toast.makeText(getApplicationContext(), "Marker Clicked ERROR :" + t.getMessage(),
                        Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        Intent backIntent = new Intent();
        backIntent.setClass(this, MainFullscreenActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(backIntent);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        initilizeMap(MAP_TYPE_TERRAIN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map, menu);
        return true;
    }

    /**
     * On selecting action bar icons
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        try
        {

            // Take appropriate action for each action item click
            switch (item.getItemId())
            {
                case R.id.about_us:
                    loadAboutUs();
                    return true;

                case R.id.terrain_view:
                    initilizeMap(MAP_TYPE_TERRAIN);
                    return true;

                case R.id.hybrid_view:
                    initilizeMap(MAP_TYPE_HYBRID);
                    return true;

                default:
                    return super.onOptionsItemSelected(item);
            }
        }
        catch (Exception e)
        {

        }
        return true;
    }

    private void loadAboutUs()
    {
        Intent intent = new Intent(getApplicationContext(), AboutUs.class);
        startActivity(intent);
    }

    public static void populateKMLData(Context context, GoogleMap googleMap, String mapDataJSONStr)
    {
        try
        {
            JSONArray mapDataArray = new JSONArray(mapDataJSONStr);
            for (int i = 0; i < mapDataArray.length(); i++)
            {
                MapData mapData = MapData.getObject(mapDataArray.getJSONObject(i));

                // add routes
                for (RoutePlacemark placemark : mapData.getRoutePlacemarks())
                {
                    PolylineOptions lineOptions = new PolylineOptions().width(8).color(Color.BLUE);
                    for (GeoCoordinate geo : placemark.getCoordinates())
                    {
                        lineOptions.add(new LatLng(geo.getLat(), geo.getLng()));
                    }
                    lineOptions.geodesic(true).visible(true).zIndex(100.0f);
                    googleMap.addPolyline(lineOptions);
                }

                // add points
                for (PointPlacemark point : mapData.getPointPlacemarks())
                {
                    googleMap.addMarker(new MarkerOptions().title(point.getName())
//                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_landmark_circle))
                        .position(new LatLng(point.getPosition().getLat(), point.getPosition().getLng())));
                }

                if (mapData.getCameraPosition() != null)
                {
                    // move camera to zoom on map
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mapData.getCameraPosition()
                        .getLat(), mapData.getCameraPosition().getLng()), mapData.getZoomLevel()));
                }

                Log.i("FW", "Map Data loaded successfully, ");
            }
        }
        catch (Exception e)
        {
            Toast.makeText(context, "Failed to load the map data :" + e.getLocalizedMessage(), Toast.LENGTH_SHORT)
                .show();
            Log.e("FW", "Error occurred populate KML Data, " + e.getLocalizedMessage());
        }
    }

}
