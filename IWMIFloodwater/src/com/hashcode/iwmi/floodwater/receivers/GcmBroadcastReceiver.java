/*
 * FILENAME
 *     GCMBroadcastReceiver.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.receivers;

import static android.content.Context.MODE_PRIVATE;
import static com.hashcode.iwmi.floodwater.MapActivity.SHARED_PREF_NAME;
import static com.hashcode.iwmi.floodwater.MapActivity.SYNC_MAP_DATA_KEY;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.hashcode.iwmi.floodwater.R;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * This will receive all the broadcast messages to this app.
 * </p>
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 * @version $Id$
 **/
public class GcmBroadcastReceiver extends BroadcastReceiver
{

    /**
     * {@inheritDoc}
     * 
     * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
     */
    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.i("FW", "Broadcast message received");

        Bundle bundle = intent.getExtras();

        String type = bundle.getString("message_type");
        if (type.equalsIgnoreCase("ALERT"))
        {
            String title = bundle.getString("title");
            String content = bundle.getString("message");

            NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_launcher) // notification icon
                    .setContentTitle(title) // title for notification
                    .setContentText(content) // message for notification
                    .setAutoCancel(true) // clear notification after click
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(content));

            NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//            mNotificationManager.notify(19861214, mBuilder.build());
        }
        else if (type.equalsIgnoreCase("SYNC_MAP"))
        {
            SharedPreferences settings = context.getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
            Editor editor = settings.edit();
            editor.putBoolean(SYNC_MAP_DATA_KEY, true);
            editor.commit();
        }
    }

}
