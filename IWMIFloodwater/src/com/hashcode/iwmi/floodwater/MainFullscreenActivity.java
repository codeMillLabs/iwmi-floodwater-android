package com.hashcode.iwmi.floodwater;

import static com.hashcode.iwmi.floodwater.MapActivity.APP_VERSION;
import static com.hashcode.iwmi.floodwater.MapActivity.GCM_REG_ID;
import static com.hashcode.iwmi.floodwater.MapActivity.SHARED_PREF_NAME;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;

import com.esri.android.runtime.ArcGISRuntime;
import com.esri.core.runtime.LicenseLevel;
import com.esri.core.runtime.LicenseResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.hashcode.iwmi.floodwater.util.HttpUtil;
import com.hashcode.iwmi.floodwater.util.SystemUiHider;

/**
 * An example full-screen activity that shows and hides the system UI (i.e. status bar and navigation/system bar) with
 * user interaction.
 * 
 * @see SystemUiHider
 */
public class MainFullscreenActivity extends Activity
{
    /**
     * Whether or not the system UI should be auto-hidden after {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = false;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after user interaction before hiding the system
     * UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise, will show the system UI visibility upon
     * interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = false;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_FULLSCREEN;
    
    // TODO: initialize CLIENT_ID with a valid client id string
    // NOTE: When you release your app, you should ensure that the client id is encrypted and saved to the device 
    // in a secure manner. 
    private static final String CLIENT_ID = "zeEH2Eqqp5CM80E0";

    // TODO: initialize LICENSE_STRING with a valid license string and set USE_LICENSE_INFO to false if you want to unlock
    // Standard license level via a license string. A license string needs to be obtained from Esri customer service.
    // NOTE: When you release your app, you should ensure that the license string is encrypted and saved to the device
    // in a secure manner. 
    private static final String LICENSE_STRING = null;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;

    private SharedPreferences settings;
    private GoogleCloudMessaging gcm;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private String regId;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_fullscreen);

        // Set the client id string on the ArcGISRuntime class. This will set the license level to Basic.
        // ArcGISRuntime.setClientId() needs to be called before any other calls to the ArcGIS SDK for Android are made.
       ArcGISRuntime.setClientId(CLIENT_ID);
        
        
        settings = getApplicationContext().getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
        regId = getRegistrationId(getApplicationContext());
        if (regId.isEmpty() && checkPlayServices())
        {
            registerInBackground(getApplicationContext());
        }

        final View controlsView = findViewById(R.id.fullscreen_content_controls);
        final View contentView = findViewById(R.id.fullscreen_content);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider.setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener()
        {
            // Cached values.
            int mControlsHeight;
            int mShortAnimTime;

            @Override
            @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
            public void onVisibilityChange(boolean visible)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
                {
                    // If the ViewPropertyAnimator API is available
                    // (Honeycomb MR2 and later), use it to animate the
                    // in-layout UI controls at the bottom of the
                    // screen.
                    if (mControlsHeight == 0)
                    {
                        mControlsHeight = controlsView.getHeight();
                    }
                    if (mShortAnimTime == 0)
                    {
                        mShortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
                    }
                    controlsView.animate().translationY(visible ? 0 : mControlsHeight).setDuration(mShortAnimTime);
                }
                else
                {
                    // If the ViewPropertyAnimator APIs aren't
                    // available, simply show or hide the in-layout UI
                    // controls.
                    controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                }

                if (visible && AUTO_HIDE)
                {
                    // Schedule a hide().
                    delayedHide(AUTO_HIDE_DELAY_MILLIS);
                }
            }
        });

        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (TOGGLE_ON_CLICK)
                {
                    mSystemUiHider.toggle();
                }
                else
                {
                    mSystemUiHider.show();
                }
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        findViewById(R.id.view_map_button).setOnTouchListener(mDelayHideTouchListener);

        findViewById(R.id.view_map_button).setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
              //                                Intent intent = new Intent(getApplicationContext(), SummaryViewActivity.class);
              //                                intent.putExtra("name", "Point 6");
              //Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                Intent intent = new Intent(getApplicationContext(), MapTabActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        //        delayedHide(100);
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the system UI. This is to prevent the jarring
     * behavior of controls going away while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            if (AUTO_HIDE)
            {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any previously scheduled calls.
     */
    private void delayedHide(int delayMillis)
    {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    private boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS)
        {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            else
            {
                Log.i("FW", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     * 
     * @return registration ID, or empty string if there is no existing registration ID.
     */
    private String getRegistrationId(Context context)
    {
        String registrationId = settings.getString(GCM_REG_ID, "");
        if (registrationId.isEmpty())
        {
            Log.i("FW", "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = settings.getInt(APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion)
        {
            Log.i("FW", "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context)
    {
        try
        {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        }
        catch (NameNotFoundException e)
        {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's shared preferences.
     */
    private void registerInBackground(final Context context)
    {
        new AsyncTask<String, Void, String>()
        {

            @Override
            protected String doInBackground(String... params)
            {
                try
                {
                    if (gcm == null)
                    {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    String regid = gcm.register(getString(R.string.gcm_project_id));

                    String url = getString(R.string.fw_server_url) + getString(R.string.fw_save_gcm_id_url);
                    Map<String, String> dataMap = new HashMap<String, String>();
                    dataMap.put("reg_id", regid);
                    return HttpUtil.sendPOSTRequest(url, dataMap);
                }
                catch (IOException ex)
                {
                    Log.e("FW", "Error occured in register in backend");
                }
                return "";
            }

            @Override
            protected void onPostExecute(String result)
            {
                try
                {
                    JSONObject json = new JSONObject(result);
                    String status = json.getString("status");
                    String regid = json.getString("reg_id");

                    if (HttpUtil.SUCCESS_STATUS_CODE.equals(status))
                        storeRegistrationId(context, regid);
                }
                catch (Exception e)
                {
                    Log.e("FW", "Error occurred in saving registration id, " + e.getLocalizedMessage());
                }
            }

        }.execute(null, null, null);
    }

    /**
     * Stores the registration ID and app versionCode in the application's {@code SharedPreferences}.
     * 
     * @param context
     *            application's context.
     * @param regId
     *            registration ID
     */
    private void storeRegistrationId(Context context, String regId)
    {
        int appVersion = getAppVersion(context);
        Log.i("FW", "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(GCM_REG_ID, regId);
        editor.putInt(APP_VERSION, appVersion);
        editor.commit();
    }
}
