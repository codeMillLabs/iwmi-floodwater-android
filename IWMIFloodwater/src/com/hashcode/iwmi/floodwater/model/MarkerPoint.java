/*
 * FILENAME
 *     MarkerPoint.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.model;

import java.io.Serializable;

import com.esri.core.geometry.Point;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Marker Point model for the ARCGIS Map
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class MarkerPoint implements Serializable
{
    private static final long serialVersionUID = 7433995445705337147L;

    private int id;
    private String name;
    private double waterLevel;
    private boolean moderate = false;
    private boolean risk = false;
    private double latitude;
    private double longitude;
    private Point point;

    public MarkerPoint(String name, double waterLevel, boolean risk, boolean moderate, double latitude, double longitude)
    {
        this.name = name;
        this.waterLevel = waterLevel;
        this.risk = risk;
        this.moderate = moderate;
        this.latitude = latitude;
        this.longitude = longitude;
        this.point = new Point(latitude, longitude);
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public double getWaterLevel()
    {
        return waterLevel;
    }

    public void setWaterLevel(double waterLevel)
    {
        this.waterLevel = waterLevel;
    }

    public boolean isModerate()
    {
        return moderate;
    }

    public void setModerate(boolean moderate)
    {
        this.moderate = moderate;
    }

    public boolean isRisk()
    {
        return risk;
    }

    public void setRisk(boolean risk)
    {
        this.risk = risk;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    public Point getPoint()
    {
        return point;
    }

    public void setPoint(Point point)
    {
        this.point = point;
    }

    @Override
    public String toString()
    {
        return "MarkerPoint [id=" + id + ", name=" + name + ", waterLevel=" + waterLevel + ", moderate=" + moderate
            + ", risk=" + risk + ", latitude=" + latitude + ", longitude=" + longitude + "]";
    }

}
