/*
 * FILENAME
 *     PointPlacemarkr.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.model;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Model class to represent Point in the map.
 * </p>
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 * 
 **/
public class PointPlacemark implements Serializable
{
    private static final long serialVersionUID = -7883732203554792196L;

    private String id;
    private String name;
    private String description;
    private GeoCoordinate position;

    /**
     * <p>
     * Get Point Place mark from JSON Object.
     * </p>
     *
     * @param json
     * @return
     * @throws JSONException
     *
     */
    public static PointPlacemark getObject(JSONObject json) throws JSONException
    {
        PointPlacemark point = new PointPlacemark();
        point.setId(json.getString("id"));
        point.setName(json.getString("name"));
        point.setDescription(json.getString("description"));
        point.setPosition(GeoCoordinate.getObject(json.getJSONObject("position")));

        return point;
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * <p>
     * Setting value for id.
     * </p>
     * 
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * <p>
     * Getter for name.
     * </p>
     * 
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * <p>
     * Setting value for name.
     * </p>
     * 
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * <p>
     * Getter for description.
     * </p>
     * 
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * <p>
     * Setting value for description.
     * </p>
     * 
     * @param description
     *            the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * <p>
     * Getter for position.
     * </p>
     * 
     * @return the position
     */
    public GeoCoordinate getPosition()
    {
        return position;
    }

    /**
     * <p>
     * Setting value for position.
     * </p>
     * 
     * @param position
     *            the position to set
     */
    public void setPosition(GeoCoordinate position)
    {
        this.position = position;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("PointPlacemarkr [id=%s, name=%s, description=%s, position=%s]", id, name, description,
            position);
    }

}
