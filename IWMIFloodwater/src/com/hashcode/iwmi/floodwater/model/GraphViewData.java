/*
 * FILENAME
 *     GraphViewData.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.model;

import android.annotation.SuppressLint;

import com.jjoe64.graphview.GraphViewDataInterface;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Graph Data model.
 * </p>
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 **/
public class GraphViewData implements GraphViewDataInterface, Comparable<GraphViewData>
{

    private double x;
    private double y;
    
    
    /**
     * <p>
     * Constructor.
     * </p>
     *
     * @param x x axis
     * @param y y axis
     *
     */
    @SuppressLint("UseValueOf") 
    public GraphViewData(long x, double y)
    {
        super();
        this.x = x;
        this.y = y;
    }

    @Override
    public double getX()
    {
        return this.x;
    }

    @Override
    public double getY()
    {
        return this.y;
    }

    @Override
    public int compareTo(GraphViewData rhs)
    {
        if(this.getX() == rhs.getX()) return 0;
        return (this.getX() < rhs.getX()) ? -1 : 1 ;
    }

}
