package com.hashcode.iwmi.floodwater;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.androidplot.xy.XYPlot;
import com.hashcode.iwmi.floodwater.tasks.WaterDataSummaryAsyncTask;
import com.hashcode.iwmi.floodwater.util.DateFormat;
import com.hashcode.iwmi.floodwater.util.FWOnTouchListener;

/**
 * <p>
 * Chart Activity for summary.
 * </p>
 * 
 * @author Amila Silva
 */
@SuppressLint({
    "FloatMath", "ClickableViewAccessibility"
})
public class SummaryChartActivity extends Activity implements OnClickListener
{
    private String measuringPoint = "";
    private TextView summaryTitle;
    private TextView todayValue;
    private TextView riskLevel;
    private ImageButton backBtn;
    private ImageButton tableViewBtn;
    private ImageButton archivesBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary_chart);

        measuringPoint = getIntent().getExtras().getString("name");

        //initialize the page content
        summaryTitle = (TextView) findViewById(R.id.summaryChartTitle);
        summaryTitle.setText(measuringPoint);
        todayValue = (TextView) findViewById(R.id.todayLevel);
        riskLevel = (TextView) findViewById(R.id.riskAlertLevel);
        
        TextView moderateText = (TextView) findViewById(R.id.moderateText);
        moderateText.setText("/ Moderate");
        
        TextView riskText = (TextView) findViewById(R.id.riskText);
        riskText.setText("/ Risk");
        
        TextView safeText = (TextView) findViewById(R.id.safeText);
        safeText.setText("/ Safe");

        backBtn = (ImageButton) findViewById(R.id.chart_back_btn);
        backBtn.setOnClickListener(this);

        tableViewBtn = (ImageButton) findViewById(R.id.table_view_btn);
        tableViewBtn.setOnClickListener(this);

        archivesBtn = (ImageButton) findViewById(R.id.archive_view_btn);
        archivesBtn.setOnClickListener(this);

        XYPlot xyPlot = loadXYPlotChart();
        FWOnTouchListener fwOnTouchListener = new FWOnTouchListener(xyPlot);
        xyPlot.setOnTouchListener(fwOnTouchListener);

        WaterDataSummaryAsyncTask asyncTask =
            new WaterDataSummaryAsyncTask(getApplicationContext(), todayValue, riskLevel, xyPlot, measuringPoint,
                fwOnTouchListener);
        asyncTask.execute(getString(R.string.fw_server_url), getString(R.string.fw_water_level_summary_url), "true",
            "4");

    }

    private XYPlot loadXYPlotChart()
    {
        XYPlot xyPlot = (XYPlot) findViewById(R.id.graph_layout);
        xyPlot.setDomainValueFormat(new DateFormat());
       /* xyPlot.getLegendWidget().position(0,
				XLayoutStyle.ABSOLUTE_FROM_RIGHT, 10,
				YLayoutStyle.ABSOLUTE_FROM_TOP, AnchorPosition.RIGHT_TOP);*/
        xyPlot.getLayoutManager().remove(xyPlot.getLegendWidget());
        xyPlot.getLayoutManager().remove(xyPlot.getTitleWidget());
        return xyPlot;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.summary_chart, menu);

        return true;
    }

    @Override
    public void onBackPressed()
    {
        Intent backIntent = new Intent();
        backIntent.setClass(this, MapTabActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(backIntent);
        finish();
        super.onBackPressed();
    }

    /**
     * On selecting action bar icons
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Take appropriate action for each action item click
        switch (item.getItemId())
        {
        //            case R.id.menu_table_view:
        //                loadSummaryTable();
        //                return true;
        //
        //            case R.id.menu_archive_chart:
        //                loadArchiveDataChart();
        //                return true;

            case R.id.about_us:
                loadAboutUs();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Launching new SummaryViewActivity
     * 
     **/
    private void loadSummaryTable()
    {
        Intent intent = new Intent(getApplicationContext(), SummaryViewActivity.class);
        //        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("name", measuringPoint);
        startActivity(intent);
        finish();
    }

    /**
     * Launching new ArchiveDataChartActivity
     * 
     **/
    private void loadArchiveDataChart()
    {
        Intent intent = new Intent(getApplicationContext(), ArchiveDataChartActivity.class);
        intent.putExtra("name", measuringPoint);
        startActivity(intent);
    }

    private void loadAboutUs()
    {
        Intent intent = new Intent(getApplicationContext(), AboutUs.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.chart_back_btn:
                onBackPressed();
                break;

            case R.id.table_view_btn:
                loadSummaryTable();
                break;

            case R.id.archive_view_btn:
                loadArchiveDataChart();
                break;

            default:
                break;
        }
    }
}
