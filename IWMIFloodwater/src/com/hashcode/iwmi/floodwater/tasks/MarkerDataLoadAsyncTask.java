/*
 * FILENAME
 *     MarkerDataLoadAsyncTask.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.tasks;

import static android.widget.Toast.LENGTH_LONG;
import static com.hashcode.iwmi.floodwater.util.HttpUtil.sendPOSTRequest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.hashcode.iwmi.floodwater.model.MarkerPoint;
import com.hashcode.iwmi.floodwater.util.HttpUtil;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Load Marker points for the Map.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class MarkerDataLoadAsyncTask extends AsyncTask<String, Void, String>
{
    private Context context;
    private List<MarkerPoint> markerPoints;
    private OnTaskComplete onTaskComplete;

    public interface OnTaskComplete
    {
        public void setMyTaskComplete(List<MarkerPoint> markerPoints);
    }

    public MarkerDataLoadAsyncTask(Context context)
    {
        this.context = context;

    }

    public void setTaskCompleteListener(OnTaskComplete onTaskComplete)
    {
        this.onTaskComplete = onTaskComplete;
    }

    @Override
    protected void onPreExecute()
    {

    }

    @Override
    protected String doInBackground(String... urls)
    {
        Map<String, String> dataMap = new HashMap<String, String>();

        long currentTime = Calendar.getInstance().getTimeInMillis();

        String url = urls[0] + urls[1];
        url = url.replace("{date}", "" + currentTime);
        return sendPOSTRequest(url, dataMap);
    }

    @Override
    protected void onPostExecute(String result)
    {
        //        Toast.makeText(context, "Data came from server, Total length :" + result.length(), Toast.LENGTH_SHORT).show();

        try
        {
            JSONObject json = new JSONObject(result);
            String status = json.getString("status");
            markerPoints = new ArrayList<MarkerPoint>();

            if (HttpUtil.ERROR_STATUS_CODE.equals(status))
            {
                Toast.makeText(context, "Marker Points loading failed.", LENGTH_LONG).show();
                return;
            }
            else if (HttpUtil.SUCCESS_STATUS_CODE.equals(status))
            {
                JSONArray dataArray = json.getJSONArray("data");
                for (int i = 0; i < dataArray.length(); i++)
                {
                    JSONObject row = dataArray.getJSONObject(i);
                    MarkerPoint mp =
                        new MarkerPoint(row.getString("name"), row.getDouble("water_level"), row.getBoolean("is_risk"),
                            row.getBoolean("is_moderate"), row.getDouble("latitude"), row.getDouble("longitude"));
                    markerPoints.add(mp);
                }
            }
            onTaskComplete.setMyTaskComplete(markerPoints);
        }
        catch (Exception e)
        {
            Log.e("FW", "Error occurred processing Response data, " + e.getLocalizedMessage());
        }
    }

}
