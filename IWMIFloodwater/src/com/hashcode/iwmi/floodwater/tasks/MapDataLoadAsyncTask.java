/*
 * FILENAME
 *     MapDataLoadAsyncTask.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.tasks;

import static android.content.Context.MODE_PRIVATE;
import static android.widget.Toast.LENGTH_LONG;
import static com.hashcode.iwmi.floodwater.MapActivity.LAST_MODIFIED_KEY;
import static com.hashcode.iwmi.floodwater.MapActivity.MAP_DATA_KEY;
import static com.hashcode.iwmi.floodwater.MapActivity.SHARED_PREF_NAME;
import static com.hashcode.iwmi.floodwater.util.HttpUtil.sendPOSTRequest;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.hashcode.iwmi.floodwater.MapActivity;
import com.hashcode.iwmi.floodwater.util.HttpUtil;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Map Data Load Async Task.
 * </p>
 * 
 * @author Amila Silva
 * 
 * @version $Id$
 **/
public class MapDataLoadAsyncTask extends AsyncTask<String, Void, String>
{
    private String mapId;
    private Context context;
    private GoogleMap googleMap;

    private SharedPreferences settings;
    private SharedPreferences.Editor editor;

    /**
     * <p>
     * Constructor MapDataLoadAsyncTask
     * </p>
     * 
     * @param context
     * @param googleMap
     * @param date
     * @param mapId
     * 
     */
    public MapDataLoadAsyncTask(Context context, GoogleMap googleMap, String mapId)
    {
        this.context = context;
        this.googleMap = googleMap;
        this.mapId = mapId;

        settings = context.getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
    }

    @Override
    protected String doInBackground(String... urls)
    {
        Map<String, String> dataMap = new HashMap<String, String>();

        long lastModifiedDate = settings.getLong(LAST_MODIFIED_KEY, 0L);

        String url = urls[0] + urls[1];
        url = url.replace("{lastModifiedTime}", "" + lastModifiedDate);
        url = url.replace("{mapId}", mapId);

        return sendPOSTRequest(url, dataMap);
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result)
    {
        //        Toast.makeText(context, "Data came from server, Total length :" + result.length(), Toast.LENGTH_SHORT).show();
        String mapDataJSONStr = settings.getString(MAP_DATA_KEY, "");
        try
        {
            JSONObject json = new JSONObject(result);
            String status = json.getString("status");

            if (HttpUtil.ERROR_STATUS_CODE.equals(status))
            {
                Toast.makeText(context, "Map Data loading failed.", LENGTH_LONG).show();
                return;
            }
            else if (HttpUtil.SUCCESS_STATUS_CODE.equals(status))
            {
                mapDataJSONStr = updateNewMapDataIfModified(mapDataJSONStr, json);
            }
        }
        catch (Exception e)
        {
            Log.e("FW", "Error occurred processing Response data, " + e.getLocalizedMessage());
        }
        finally
        {
            // populateKMLData(context, googleMap, mapDataJSONStr);
        }
    }

    private String updateNewMapDataIfModified(String mapDataJSONStr, JSONObject json) throws JSONException
    {
        boolean isModified = json.getBoolean("modified");
        if (isModified)
        {
            editor = settings.edit();
            editor.putLong(LAST_MODIFIED_KEY, json.getLong("last_modified_date"));
            editor.putBoolean(MapActivity.SYNC_MAP_DATA_KEY, false);
            if (json.getInt("map_data_size") > 0)
            {
                mapDataJSONStr = json.getJSONArray(MAP_DATA_KEY).toString();
                Log.d("FW-HTTP", "Get Map Data JSON string, " + mapDataJSONStr);
                editor.putString(MAP_DATA_KEY, mapDataJSONStr);
            }
            editor.commit();
        }
        return mapDataJSONStr;
    }
}
