/*
 * FILENAME
 *     WaterDataSummeryAsyncTask.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.tasks;

import static android.widget.Toast.LENGTH_LONG;
import static com.hashcode.iwmi.floodwater.util.HttpUtil.sendPOSTRequest;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.hashcode.iwmi.floodwater.R;
import com.hashcode.iwmi.floodwater.SummaryViewActivity;
import com.hashcode.iwmi.floodwater.model.GraphViewData;
import com.hashcode.iwmi.floodwater.util.FWLineAndPointFormatter;
import com.hashcode.iwmi.floodwater.util.FWOnTouchListener;
import com.hashcode.iwmi.floodwater.util.HttpUtil;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Water data summary and prediction data Async Task for chart and summary information.
 * </p>
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 * 
 **/
public class WaterDataSummaryAsyncTask extends AsyncTask<String, Void, String>
{
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private String pointName;
    private static Context context;
    private TextView todayValue;
    private TextView riskValue;
    private TableLayout viewTable;
    private XYPlot xyPlot;
    private boolean isArchiveTable;
    private FWOnTouchListener onTouchListener;
    
    private static final DecimalFormat numFormat = new DecimalFormat("#.00");

    /**
     * <p>
     * Constructor for Table view WaterDataSummeryAsyncTask
     * </p>
     * 
     * @param context
     *            application context
     * @param pointName
     *            point name to search data
     * 
     */
    public WaterDataSummaryAsyncTask(Context context, TextView todayValue, TextView riskValue, TableLayout viewTable,
        String pointName)
    {
        this.context = context;
        this.pointName = pointName;
        this.viewTable = viewTable;
        this.todayValue = todayValue;
        this.riskValue = riskValue;
    }

    /**
     * <p>
     * Constructor for graph view WaterDataSummeryAsyncTask
     * </p>
     * 
     * @param context
     *            application context
     * @param pointName
     *            point name to search data
     * 
     */
    public WaterDataSummaryAsyncTask(Context context, TextView todayValue, TextView riskValue, XYPlot xyPlot,
        String pointName, FWOnTouchListener onTouchListener)
    {
        this.context = context;
        this.pointName = pointName;
        this.todayValue = todayValue;
        this.riskValue = riskValue;
        this.xyPlot = xyPlot;
        this.onTouchListener = onTouchListener;
    }

    /**
     * <p>
     * Constructor for graph view WaterDataSummeryAsyncTask
     * </p>
     * 
     * @param context
     *            application context
     * @param pointName
     *            point name to search data
     * 
     */
    public WaterDataSummaryAsyncTask(Context context, XYPlot xyPlot, String pointName, boolean isArchivedData,
        FWOnTouchListener onTouchListener)
    {
        this.context = context;
        this.pointName = pointName;
        this.xyPlot = xyPlot;
        this.isArchiveTable = isArchivedData;
        this.onTouchListener = onTouchListener;
    }

    @Override
    protected String doInBackground(String... params)
    {
        Map<String, String> dataMap = new HashMap<String, String>();
        dataMap.put("date", sdf.format(new Date()));
        dataMap.put("location_id", pointName);
        dataMap.put("date_in_milis", params[2]);
        dataMap.put("no_past_days", params[3]);

        String url = params[0] + params[1];
        return sendPOSTRequest(url, dataMap);
    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result)
    {
        //        Toast.makeText(context, "Data came from server, Total length :" + result.length(), Toast.LENGTH_SHORT).show();
        try
        {
            JSONObject json = new JSONObject(result);
            String status = json.getString("status");

            if (HttpUtil.ERROR_STATUS_CODE.equals(status))
            {
                Toast.makeText(context, "Water level forcasting loading failed", LENGTH_LONG).show();
                return;
            }
            else if (HttpUtil.SUCCESS_STATUS_CODE.equals(status))
            {
                if (viewTable != null)
                    loadDataForSummary(json);
                else if (!isArchiveTable)
                    loadDataForGraph(json);
                else
                {
                    loadArchiveDataForGraph(json);
                }
            }
        }
        catch (Exception e)
        {
            Toast.makeText(context, "Water level forcasting data loading failed", LENGTH_LONG).show();
            Log.e("FW", "Error occurred processing Response data, " + e.getLocalizedMessage());
        }
    }

    private void loadDataForSummary(JSONObject json) throws JSONException
    {

        double risk = json.has("threshold_value") ? json.getDouble("threshold_value") : 1000.0;
        double moderate = json.has("moderate_value") ? json.getDouble("moderate_value") : 1000.0;

        int textBgColor = Color.parseColor(context.getString(R.color.actual_bg_color));

        JSONArray dataArray = json.getJSONArray("actual_data");
        for (int i = 0; i < dataArray.length(); i++)
        {
            JSONObject row = dataArray.getJSONObject(i);
            String label = row.getString("label");
            double value = row.getDouble("value");
            int textColor = Color.parseColor(context.getString(R.color.black_text_color)); //getTextBgColor(value, risk, moderate);
            //            textBgColor = Color.parseColor(context.getString(R.color.actual_bg_color));
            viewTable.addView(
                SummaryViewActivity.createTableRow(context, "ACTUAL", i, label, value, textColor, textBgColor),
                SummaryViewActivity.tableLayoutParam);
        }
        // Add Today
        JSONObject today = json.getJSONObject("today");
        double todayWaterLevelValue = today.getDouble("value");
        int textColor = getTextColor(todayWaterLevelValue, risk, moderate);

        viewTable.addView(SummaryViewActivity.createTableRow(context, "TODAY", 0, "Today", todayWaterLevelValue,
            textColor, textBgColor), SummaryViewActivity.tableLayoutParam);

        // Forcasting data
        dataArray = json.getJSONArray("forcasting_data");
        for (int i = 0; i < dataArray.length(); i++)
        {
            JSONObject row = dataArray.getJSONObject(i);
            String label = row.getString("label");
            double value = row.getDouble("value");
            textColor = Color.parseColor(context.getString(R.color.black_text_color)); //getTextColor(value, risk, moderate);
            //            textBgColor = Color.parseColor(context.getString(R.color.forecasting_bg_color));
            viewTable.addView(
                SummaryViewActivity.createTableRow(context, "FORCAST", i, label, value, textColor, textBgColor),
                SummaryViewActivity.tableLayoutParam);
        }

        setTodayStatus(todayWaterLevelValue, risk, moderate);

    }

    private int getTextColor(double waterLevelValue, double risk, double moderate)
    {
        if (waterLevelValue >= risk)
        {
            return Color.parseColor(context.getString(R.color.graph_above_threshold_color));
        }
        else if (waterLevelValue >= moderate)
        {
            return Color.parseColor(context.getString(R.color.graph_moderate_color));
        }
        else
        {
            return Color.parseColor(context.getString(R.color.graph_normal_color));
        }
    }

    private int getTextBgColor(double waterLevelValue, double risk, double moderate)
    {
        if (waterLevelValue >= risk)
        {
            return Color.parseColor(context.getString(R.color.summary_above_threshold_bg_color));
        }
        else if (waterLevelValue >= moderate)
        {
            return Color.parseColor(context.getString(R.color.summary_moderate_bg_color));
        }
        else
        {
            return Color.parseColor(context.getString(R.color.summary_normal_bg_color));
        }
    }
    
    private void setTodayStatus(double todayWaterLevelValue, double risk, double moderate)
    {
        todayValue.setText("Today : " + numFormat.format(todayWaterLevelValue) + "m ");

        if (todayWaterLevelValue >= risk)
        {
            riskValue.setText(context.getString(R.string.risk_high_risk));
            riskValue.setTextColor(Color.parseColor(context.getString(R.color.graph_above_threshold_color)));
        }
        else if (todayWaterLevelValue >= moderate)
        {
            riskValue.setText(context.getString(R.string.risk_moderate));
            riskValue.setTextColor(Color.parseColor(context.getString(R.color.graph_moderate_color)));
        }
        else
        {
            riskValue.setText(context.getString(R.string.risk_safe));
            riskValue.setTextColor(Color.parseColor(context.getString(R.color.graph_normal_color)));
        }
    }

    private void loadDataForGraph(JSONObject json) throws JSONException
    {
        double risk = json.has("threshold_value") ? json.getDouble("threshold_value") : 1000.0;
        double moderate = json.has("moderate_value") ? json.getDouble("moderate_value") : 1000.0;

        JSONArray dataArray = json.getJSONArray("actual_data");
        ArrayList<GraphViewData> graphData = new ArrayList<GraphViewData>();

        for (int i = 0; i < dataArray.length(); i++)
        {
            JSONObject row = dataArray.getJSONObject(i);
            graphData.add(new GraphViewData(row.getLong("label"), row.getDouble("value")));
        }
        // Add Today
        JSONObject today = json.getJSONObject("today");
        double todayWaterLevelValue = today.getDouble("value");
        graphData.add(new GraphViewData(today.getLong("label"), todayWaterLevelValue));

        // Forcasting data
        dataArray = json.getJSONArray("forcasting_data");
        for (int i = 0; i < dataArray.length(); i++)
        {
            JSONObject row = dataArray.getJSONObject(i);
            graphData.add(new GraphViewData(row.getLong("label"), row.getDouble("value")));
        }
        Collections.sort(graphData);

        setTodayStatus(todayWaterLevelValue, risk, moderate);

        drawGraph(risk, moderate, graphData);
    }

    private void loadArchiveDataForGraph(JSONObject json) throws JSONException
    {
        double risk = json.has("threshold_value") ? json.getDouble("threshold_value") : 1000.0;
        double moderate = json.has("moderate_value") ? json.getDouble("moderate_value") : 1000.0;

        JSONArray dataArray = json.getJSONArray("actual_data");
        ArrayList<GraphViewData> graphData = new ArrayList<GraphViewData>();

        for (int i = 0; i < dataArray.length(); i++)
        {
            JSONObject row = dataArray.getJSONObject(i);
            graphData.add(new GraphViewData(row.getLong("label"), row.getDouble("value")));
        }
        // Add Today
        JSONObject today = json.getJSONObject("today");
        graphData.add(new GraphViewData(today.getLong("label"), today.getDouble("value")));

        Collections.sort(graphData);

        // Data loading for graph
        drawGraph(risk, moderate, graphData);
    }

    private void drawGraph(double risk, double moderate, ArrayList<GraphViewData> graphData)
    {
        try
        {
            Number[] dates = new Number[graphData.size()];
            Number[] levels = new Number[graphData.size()];

            Number[] moderates = new Number[graphData.size()];
            Number[] risks = new Number[graphData.size()];

            for (int i = 0; i < graphData.size(); i++)
            {
                dates[i] = graphData.get(i).getX() / 1000;
                levels[i] = graphData.get(i).getY();
                risks[i] = risk;
                moderates[i] = moderate;
            }

            XYSeries series = new SimpleXYSeries(Arrays.asList(dates), Arrays.asList(levels), "Graph");
            XYSeries moderateSeries = new SimpleXYSeries(Arrays.asList(dates), Arrays.asList(moderates), "Mod");
            XYSeries riskSeries = new SimpleXYSeries(Arrays.asList(dates), Arrays.asList(risks), "Risk");
            FWLineAndPointFormatter formatter = new FWLineAndPointFormatter(moderate, risk);
            LineAndPointFormatter moderateFormatter =
                new LineAndPointFormatter(Color.parseColor(context
                    .getString(R.color.graph_moderate_threshold_line_color)), null, null, null);
            LineAndPointFormatter riskFormatter =
                new LineAndPointFormatter(
                    Color.parseColor(context.getString(R.color.graph_above_threshold__line_color)), null, null, null);

            xyPlot.addSeries(moderateSeries, moderateFormatter);
            xyPlot.addSeries(riskSeries, riskFormatter);
            xyPlot.addSeries(series, formatter);
            xyPlot.redraw();

            xyPlot.calculateMinMaxVals();
            onTouchListener.setMinXY(new PointF(xyPlot.getCalculatedMinX().floatValue(), xyPlot.getCalculatedMinY()
                .floatValue()));
            onTouchListener.setMaxXY(new PointF(xyPlot.getCalculatedMaxX().floatValue(), xyPlot.getCalculatedMaxY()
                .floatValue()));
        }
        catch (Exception ex)
        {

        }
    }

    public void setArchiveTable(boolean isArchiveTable)
    {
        this.isArchiveTable = isArchiveTable;
    }
}
