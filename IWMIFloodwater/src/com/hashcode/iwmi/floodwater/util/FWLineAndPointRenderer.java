package com.hashcode.iwmi.floodwater.util;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;

import com.androidplot.util.PixelUtils;
import com.androidplot.util.ValPixConverter;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.LineAndPointRenderer;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

/**
 * @author Manuja
 */
public class FWLineAndPointRenderer extends
		LineAndPointRenderer<FWLineAndPointFormatter> {

	private static final float DEFAULT_LINE_STROKE_WIDTH_DP = 2.0f;

	private Paint highPaint;
	private Paint moderatePaint;
	private Paint safePaint;

	public FWLineAndPointRenderer(XYPlot plot) {
		super(plot);

		safePaint = createPaint(Color.BLUE);
		highPaint = createPaint(Color.RED);
		moderatePaint = createPaint(Color.argb(255, 250, 147, 17)); //Orange
	}

	/**
	 * Overridden draw method to get the "vertex stroke" effect. 99% of this is
	 * copy/pasted from the super class' implementation.
	 * 
	 * @param canvas
	 * @param plotArea
	 * @param series
	 * @param formatter
	 */
	@Override
	protected void drawSeries(Canvas canvas, RectF plotArea, XYSeries series,
			LineAndPointFormatter formatter) {

		for (int i = 1; i < series.size(); i++) {
			Number y1 = series.getY(i - 1);
			Number x1 = series.getX(i - 1);

			Number y2 = series.getY(i);
			Number x2 = series.getX(i);

			PointF point1 = ValPixConverter.valToPix(x1, y1, plotArea,
					getPlot().getCalculatedMinX(), getPlot()
							.getCalculatedMaxX(),
					getPlot().getCalculatedMinY(), getPlot()
							.getCalculatedMaxY());

			PointF point2 = ValPixConverter.valToPix(x2, y2, plotArea,
					getPlot().getCalculatedMinX(), getPlot()
							.getCalculatedMaxX(),
					getPlot().getCalculatedMinY(), getPlot()
							.getCalculatedMaxY());

			formatter.setLinePaint(getColor(formatter, y1.floatValue(),
					y2.floatValue()));

			Path pathx = new Path();
			pathx.moveTo(point1.x, point1.y);
			appendToPath(pathx, point2, point1);

			renderPath(canvas, plotArea, pathx, point1, point2, formatter);
		}
	}

	private Paint getColor(LineAndPointFormatter formatter, float value1,
			float value2) {
		if (value2 < ((FWLineAndPointFormatter) formatter)
				.getModerateSafeThreshold()) {
			return safePaint;
		} else if (value2 > ((FWLineAndPointFormatter) formatter)
				.getHighModerateThreshold()) {
			return highPaint;
		} else {
			return moderatePaint;
		}
	}

	private Paint createPaint(int color) {
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setStrokeWidth(PixelUtils.dpToPix(DEFAULT_LINE_STROKE_WIDTH_DP));
		paint.setColor(color);
		paint.setStyle(Paint.Style.STROKE);

		return paint;
	}
}
