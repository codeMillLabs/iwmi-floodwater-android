package com.hashcode.iwmi.floodwater.util;

import android.graphics.Color;

import com.androidplot.ui.SeriesRenderer;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.XYPlot;

/**
 * @author Manuja
 */
@SuppressWarnings("rawtypes")
public class FWLineAndPointFormatter extends LineAndPointFormatter
{
    private double highModerateThreshold;
    private double moderateSafeThreshold;

    public FWLineAndPointFormatter(double moderateSafeThreshold, double highModerateThreshold)
    {
        super(Color.BLUE, Color.RED, null, null);

        this.moderateSafeThreshold = moderateSafeThreshold;
        this.highModerateThreshold = highModerateThreshold;
    }

    @Override
    public Class<? extends SeriesRenderer> getRendererClass()
    {
        return FWLineAndPointRenderer.class;
    }

    @Override
    public SeriesRenderer getRendererInstance(XYPlot plot)
    {
        return new FWLineAndPointRenderer(plot);
    }

    /**
     * @return the highModerateThreshold
     */
    public double getHighModerateThreshold()
    {
        return highModerateThreshold;
    }

    /**
     * @return the moderateSafeThreshold
     */
    public double getModerateSafeThreshold()
    {
        return moderateSafeThreshold;
    }
}
