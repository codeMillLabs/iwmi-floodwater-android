package com.hashcode.iwmi.floodwater.util;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.XYPlot;

import android.annotation.SuppressLint;
import android.graphics.PointF;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

@SuppressLint({ "ClickableViewAccessibility", "FloatMath" })
public class FWOnTouchListener implements OnTouchListener {

	private PointF minXY;
	private PointF maxXY;
	static final int NONE = 0;
	static final int ONE_FINGER_DRAG = 1;
	static final int TWO_FINGERS_DRAG = 2;
	int mode = NONE;
	PointF firstFinger;
	float distBetweenFingers;
	boolean stopThread = false;
	private XYPlot xyPlot;
	
	public FWOnTouchListener(XYPlot xyPlot)
	{
		this.xyPlot= xyPlot; 
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN: // Start gesture
			firstFinger = new PointF(event.getX(), event.getY());
			mode = ONE_FINGER_DRAG;
			stopThread = true;
			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP:
			mode = NONE;
			break;
		case MotionEvent.ACTION_POINTER_DOWN: // second finger
			distBetweenFingers = spacing(event);
			// the distance check is done to avoid false alarms
			if (distBetweenFingers > 5f) {
				mode = TWO_FINGERS_DRAG;
			}
			break;
		case MotionEvent.ACTION_MOVE:
			if (mode == ONE_FINGER_DRAG) {
				PointF oldFirstFinger = firstFinger;
				firstFinger = new PointF(event.getX(), event.getY());
				scroll(oldFirstFinger.x - firstFinger.x);
				xyPlot.setDomainBoundaries(minXY.x, maxXY.x, BoundaryMode.FIXED);
				xyPlot.redraw();

			} else if (mode == TWO_FINGERS_DRAG) {
				float oldDist = distBetweenFingers;
				distBetweenFingers = spacing(event);
				zoom(oldDist / distBetweenFingers);
				xyPlot.setDomainBoundaries(minXY.x, maxXY.x, BoundaryMode.FIXED);
				xyPlot.redraw();
			}
			break;
		}
		return true;
	}

	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return FloatMath.sqrt(x * x + y * y);
	}

	private void scroll(float pan) {
		float domainSpan = maxXY.x - minXY.x;
		float step = domainSpan / xyPlot.getWidth();
		float offset = pan * step;
		minXY.x = minXY.x + offset;
		maxXY.x = maxXY.x + offset;
	}

	/**
	 * @param minXY the minXY to set
	 */
	public void setMinXY(PointF minXY) {
		this.minXY = minXY;
	}

	/**
	 * @param maxXY the maxXY to set
	 */
	public void setMaxXY(PointF maxXY) {
		this.maxXY = maxXY;
	}

	private void zoom(float scale) {
		float domainSpan = maxXY.x - minXY.x;
		float domainMidPoint = maxXY.x - domainSpan / 2.0f;
		float offset = domainSpan * scale / 2.0f;

		minXY.x = domainMidPoint - offset;
		maxXY.x = domainMidPoint + offset;
	}

}
