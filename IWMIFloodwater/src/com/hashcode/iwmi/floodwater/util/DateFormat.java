package com.hashcode.iwmi.floodwater.util;

import android.annotation.SuppressLint;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class DateFormat  extends Format{
	
	private static final long serialVersionUID = 6897593018241016856L;
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yy");

	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo,
			FieldPosition pos) {
		long timestamp = ((Number) obj).longValue() * 1000;
		Date date = new Date(timestamp);
		return sdf.format(date, toAppendTo, pos);
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		return null;
	}
}
