package com.hashcode.iwmi.floodwater;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * <p>
 * About Us Activity.
 * </p>
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 * 
 */
public class AboutUs extends Activity implements OnClickListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        findViewById(R.id.credit_back_btn).setOnClickListener(this);
        findViewById(R.id.map_view_btn).setOnClickListener(this);
    }

    @Override
    public void onBackPressed()
    {
        Intent backIntent = new Intent();
        backIntent.setClass(this, MapTabActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        backIntent.putExtra("tab_index", 0);
        startActivity(backIntent);
        finish();
        super.onBackPressed();
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.credit_back_btn:
                onBackPressed();
                break;

            case R.id.map_view_btn:
                loadMap();
                break;

            default:
                break;
        }
    }

    private void loadMap()
    {
        Intent backIntent = new Intent();
        backIntent.setClass(this, MapTabActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(backIntent);
        finish();

    }
}
