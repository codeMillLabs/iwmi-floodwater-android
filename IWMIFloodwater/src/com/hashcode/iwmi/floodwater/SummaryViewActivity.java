package com.hashcode.iwmi.floodwater;

import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.hashcode.iwmi.floodwater.tasks.WaterDataSummaryAsyncTask;

/**
 * 
 * <p>
 * Summary view Activity
 * </p>
 * 
 * @author Amila Silva
 * 
 * 
 */
public class SummaryViewActivity extends Activity implements OnClickListener
{
    public static LayoutParams tableLayoutParam =
        new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

    private TextView summaryTitle;
    private TextView todayValue;
    private TextView riskLevel;
    private TableLayout viewTable;
    private ImageButton backBtn;
    private ImageButton chartBtn;
    private String measuringPoint = "";
    
    private static final DecimalFormat numFormat = new DecimalFormat("#.00");

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary_view);

        measuringPoint = getIntent().getExtras().getString("name");

        //initialize the page content
        todayValue = (TextView) findViewById(R.id.todayLevel);
        riskLevel = (TextView) findViewById(R.id.riskAlertLevel);
        summaryTitle = (TextView) findViewById(R.id.summaryViewTitle);
        summaryTitle.setText(measuringPoint);

        backBtn = (ImageButton) findViewById(R.id.summary_back_btn);
        backBtn.setOnClickListener(this);

        chartBtn = (ImageButton) findViewById(R.id.chart_view_btn);
        chartBtn.setOnClickListener(this);

        viewTable = (TableLayout) findViewById(R.id.viewDataTable);
        TableRow trHead = createHeaderRow();

        WaterDataSummaryAsyncTask asyncTask =
            new WaterDataSummaryAsyncTask(getApplicationContext(), todayValue, riskLevel, viewTable, measuringPoint);
        asyncTask.execute(getString(R.string.fw_server_url), getString(R.string.fw_water_level_summary_url), "false",
            "4");

        viewTable.addView(trHead, new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
    }

    private TableRow createHeaderRow()
    {
        TableRow trHead = new TableRow(this);
        trHead.setWeightSum(1);
        trHead.setBackgroundColor(Color.parseColor(getString(R.color.table_header_color)));
        trHead.setLayoutParams(tableLayoutParam);
        trHead.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView date = new TextView(this);
        date.setText(getString(R.string.table_header_date));
        date.setTextSize(20.0f);
        date.setTextColor(Color.WHITE);
        date.setGravity(Gravity.CENTER);
        trHead.addView(date);

        TextView waterLevel = new TextView(this);
        waterLevel.setText(getString(R.string.table_header_water_level));
        waterLevel.setTextSize(20.0f);
        waterLevel.setTextColor(Color.WHITE);
        waterLevel.setGravity(Gravity.CENTER);
        trHead.addView(waterLevel);

        return trHead;
    }

    
    public static TableRow createTableRow(Context context, String type, int rowCount, String date, double waterLevel,
       int textColr, int textBgColor)
    {
        //        int textColor = ("ACTUAL".equals(type)) ? Color.BLUE : Color.MAGENTA;
        TableRow tr = new TableRow(context);
        tr.setWeightSum(1);
//        tr.setBackgroundColor(textBgColor);
        int textColor = textColr;

        if ("TODAY".equals(type))
        {
            tr.setBackgroundColor(Color.parseColor(context.getString(R.color.today_row_color)));
            textColor = Color.BLACK;
        } 
//        else if (rowCount % 2 == 0)
//        {
//            tr.setBackgroundColor(Color.parseColor(context.getString(R.color.odd_row_color)));
//        }
//        else
//        {
//            tr.setBackgroundColor(Color.TRANSPARENT);
//        }

        tr.setLayoutParams(tableLayoutParam);
        tr.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView dateRow = new TextView(context);
        dateRow.setText(date);
        dateRow.setTextSize(16.0f);
        dateRow.setTextColor(textColor);
        dateRow.setGravity(Gravity.CENTER);
        tr.addView(dateRow);

        TextView waterLevelRow = new TextView(context);
        waterLevelRow.setText(numFormat.format(waterLevel));
        waterLevelRow.setTextColor(textColor);
        waterLevelRow.setTextSize(16.0f);
        waterLevelRow.setGravity(Gravity.CENTER);
        tr.addView(waterLevelRow);
        return tr;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
//        getMenuInflater().inflate(R.menu.summary_view, menu);
        return true;
    }

    @Override
    public void onBackPressed()
    {
        Intent backIntent = new Intent();
        backIntent.setClass(this, MapTabActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(backIntent);
        finish();
        super.onBackPressed();
    }

    /**
     * On selecting action bar icons
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.about_us:
                loadAboutUs();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Launching new SummaryViewActivity
     * 
     **/
    private void loadChart()
    {
//        Intent intent = new Intent(getApplicationContext(), SummaryChartActivity.class);
        Intent intent = new Intent(getApplicationContext(), ArchiveDataChartActivity.class);
        intent.putExtra("name", measuringPoint);
        startActivity(intent);
        finish();
    }

    private void loadAboutUs()
    {
        Intent intent = new Intent(getApplicationContext(), AboutUs.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.summary_back_btn:
                onBackPressed();
                break;

            case R.id.chart_view_btn:
                loadChart();
                break;

            default:
                break;
        }
    }
}
